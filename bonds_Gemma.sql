SELECT * FROM bond WHERE CUSIP ='28717RH95'

SELECT * FROM bond ORDER BY maturity

SELECT SUM(quantity*price) AS TotValue FROM bond

SELECT quantity*coupon /100 AS anualValue FROM bond

SELECT b.CUSIP, b.rating, r.ordinal  FROM bond AS b
Inner join rating as r
ON r.rating = b.rating
WHERE ordinal <= 3

SELECT AVG(price) AS avePrice, AVG(coupon) AS AveCoup, rating FROM bond
group by rating

SELECT CUSIP, coupon/price AS yield, expected_yield FROM bond
INNER JOIN rating
ON bond.rating = rating.rating
WHERE expected_yield > (coupon / price)


