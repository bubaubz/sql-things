SELECT opening.stock, opening.instant, trader.first_name, trader.last_name FROM trader
INNER JOIN position
ON trader.ID = position.trader_ID
INNER JOIN trade AS opening
ON position.opening_trade_ID = opening.ID
INNER JOIN trade AS closing
ON position.closing_trade_ID = closing.ID
WHERE trader.ID = 1 AND opening.stock = 'BOM'

SELECT SUM(trade.size*trade.price*(CASE WHEN trade.buy = 1 then 1 ELSE -1 END)) AS movingMoney
FROM trade
INNER JOIN position as opening
ON opening.opening_trade_id = trade.id
WHERE opening.trader_id = 1 
AND trade.stock = 'MRK'
AND opening.closing_trade_id != opening.opening_trade_id
AND opening.closing_trade_id is not null

create view traders_profits_losses as
select
  tr.first_name, tr.last_name,
  sum(t.size * t.price * (case when t.buy = 1 then -1 else 1 end)) as 'profit'
from trade t
  join position p on p.opening_trade_id = t.id or p.closing_trade_id = t.id
  join trader tr on p.trader_id = tr.id
where p.closing_trade_id is not null
  and p.closing_trade_id != p.opening_trade_id
group by tr.first_name, tr.last_name;